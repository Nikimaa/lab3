const submit = document.querySelector('button[type=submit]');
const reset = document.querySelector('button[type=reset]');
const fieldset = document.querySelector('form').elements.jscheck
const formElements = fieldset.elements;

fieldset.addEventListener('change', function(e){
    if(e.target.tagName != "TEXTAREA") checkElem(e.target);
    checkForm();
});

document.addEventListener("DOMContentLoaded", function(){
    checkForm();
});
reset.addEventListener('click', function(){
    submit.disabled = true;
});

function checkElem(elem){
    if(elem.value.length >= 2){
        elem.classList.remove("is-invalid");
        elem.classList.add("is-valid");
    }
    else{
        elem.classList.remove("is-valid");
        elem.classList.add("is-invalid");
    }
}

function checkForm(){
    for(let elem of formElements){
        if(elem.tagName == "TEXTAREA") continue;
        if(elem.value.length < 2){
            submit.disabled = true;
            return;
        }
    }
    submit.disabled = false;
}

const imgLoader = document.querySelector("input[type=file]");
const avatar = document.querySelector('.avatar');

imgLoader.addEventListener('change', changeHandler);

function changeHandler(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var files = evt.target.files;
    var file = files[0];
    var fileReader = new FileReader();
 
 
    fileReader.onload = function(progressEvent) {
        var url = fileReader.result;
 
        console.log(url);
        avatar.style.backgroundImage = `url(${url})`;
    }

    fileReader.readAsDataURL(file);
}