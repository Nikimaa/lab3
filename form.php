<?php
    $errors = [];
    if(sizeof($_REQUEST) > 0){
        $errors["adres"] = strlen($_REQUEST["adres"]) <= 6;
        $errors["skype"] = strlen($_REQUEST["skype"]) <= 4;
        $errors["tel"] =   strlen($_REQUEST["tel"]) <= 12; 
        $errors["email"] = filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL) ? false : true;

        $validation = false;
        foreach($errors as $error){
            $validation = $validation || $error;
        }


        if(!$validation){
            echo "Registration succes";
            $formJson = json_encode($_REQUEST);
            file_put_contents('form.json', $formJson);
        }
        else{
            require('index.php');
        }
    }
?>