<?php 
    $jsonString = file_get_contents("form.json");
    $formData = json_decode($jsonString);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
        <script src="https://kit.fontawesome.com/56c7ba5ce2.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="center">
                <h1><i class="far fa-user"></i> Мой профиль</h1>
                <form name="ff" action="form.php" method="get">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="avatar"></div>
                            <input type="file" class="form-control-file" name="avatar"/>
                        </div>
                        <div class="col-md-5">
                            <fieldset name="jscheck">
                                <div>
                                    <label for="surname">Фамилия</label>
                                    <input type="text" name="surname" class="form-control form-control-sm" placeholder="Фамилия" id="surname" 
                                    value="<?php 
                                    if(sizeof($_REQUEST) > 0){
                                        echo $_REQUEST["surname"];
                                    }
                                    else{
                                        echo $formData->surname;
                                    }  
                                    ?>"/>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">
                                        Фамилия должна содержать 2 и более символов
                                    </div>
                                </div>

                                <div>
                                    <label for="name">Имя</label>
                                    <input type="text" name="name" class="form-control form-control-sm" placeholder="Имя" id="name" 
                                    value="<?php
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["name"]; 
                                    }
                                    else{
                                        echo $formData->name;
                                    }
                                    ?>"/>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">
                                        Имя должно содержать 2 и более символов
                                    </div>
                                </div>

                                <div>
                                    <label for="patron">Отчество</label>
                                    <input type="text" name="patron" class="form-control form-control-sm" placeholder="Отчество" id="patron" value="<?php
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["patron"]; 
                                    }
                                    else{
                                        echo $formData->patron;
                                    }
                                    ?>"/>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">
                                        Отчество должно содержать 2 и более символов
                                    </div>
                                </div>

                                <label for="datebirth">Дата рождения</label>
                                <input type="date" name="datebirth" class="form-control form-control-sm" id="datebirth" value="<?php
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["datebirth"]; 
                                    }
                                    else{
                                        echo $formData->datebirth;
                                    }
                                    ?>"/>
                                <div class="valid-feedback"></div>
                                <div class="invalid-feedback">
                                    Неверный формат даты
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="gender">Пол</label>
                                        <select name="gender" class="form-control form-control-sm" id="gender" >
                                            <option value="">-</option>
                                            <option value="мужской">мужской</option>
                                            <option value="женский">женский</option>
                                        </select>
                                        <div class="valid-feedback"></div>
                                        <div class="invalid-feedback">
                                            Пожалуйста выберете пол
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="nation">Национальность</label>
                                        <select name="nation" class="form-control form-control-sm" id="nation">
                                            <option value="">-</option>
                                            <option value="Украина">Украина</option>
                                            <option value="Россия">Россия</option>
                                            <option value="Россия">Белорусия</option>
                                        </select>
                                        <div class="valid-feedback"></div>
                                        <div class="invalid-feedback">
                                            Пожалуйста выберете национальность
                                        </div>
                                    </div>
                                </div>
                                <label for="shinfo">Краткая информация</label>
                                <textarea name="shinfo" class="form-control form-control-lg ta" id="shinfo">
                                <?php 
                                if(sizeof($_REQUEST) > 0){
                                    echo $_REQUEST["shinfo"]; 
                                }
                                else{
                                    echo $formData->shinfo;
                                }?>
                                    </textarea>
                            </fieldset>
                        </div>
                        <div class="col-md-5">
                            <label for="adres">Адрес</label>
                            <input type="text" name="adres" class="form-control form-control-sm <?php echo $errors["adres"] ? "is-invalid" : "is-valid" ?>" placeholder="Адрес" id="adres" value="<?php
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["adres"]; 
                                    }
                                    else{
                                        echo $formData->adres;
                                    }
                                    ?>"/>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">
                                Адрес должен содеражать более 6 символов
                            </div>

                            Мои увлечения
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="Баскетбол" id="c1" />
                                <label class="form-check-label" for="c1">
                                    Баскетбол
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="Футбол" id="c2" />
                                <label class="form-check-label" for="c2">
                                    Футбол
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="Волейбол" id="c3" />
                                <label class="form-check-label" for="c3">
                                    Волейбол
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="Хоккей" id="c4" />
                                <label class="form-check-label" for="c4">
                                    Хоккей
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control form-control-sm <?php echo $errors["email"] ? "is-invalid" : "is-valid" ?>" placeholder="Email" id="email" value="<?php
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["email"]; 
                                    }
                                    else{
                                        echo $formData->email;
                                    }
                                    ?>"/>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">
                                        Почта не соответствует формату
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="skype">Skype</label>
                                    <input type="text" name="skype" class="form-control form-control-sm <?php echo $errors["skype"] ? "is-invalid" : "is-valid" ?>" placeholder="Skype" id="skype" value="<?php 
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["skype"]; 
                                    }
                                    else{
                                        echo $formData->skype;
                                    } ?>"/>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">
                                        Логин для Skype должен содержать больше 4 символов
                                    </div>
                                </div>
                            </div>
                            <div class="">
                            <label for="tel">Телефон</label>
                            <input type="tel" name="tel" class="form-control form-control-sm <?php echo $errors["tel"] ? "is-invalid" : "is-valid" ?>" id="tel" value="<?php 
                                    if(sizeof($_REQUEST) > 0){
                                      echo $_REQUEST["tel"]; 
                                    }
                                    else{
                                        echo $formData->tel;
                                    } ?>"/>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">
                                Телефон не соответствует формату
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Связываться со мной:
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="телефон" id="r1" name="contact_by" checked />
                                        <label class="form-check-label" for="r1">
                                            По телефону
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="почта" id="r2" name="contact_by" />
                                        <label class="form-check-label" for="r2">
                                            По электронной почте
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="skype" id="r3" name="contact_by" />
                                        <label class="form-check-label" for="r3">
                                            Skype
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Присылать уведомления
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="да" id="r4" name="r2" checked />
                                        <label class="form-check-label" for="r4">
                                            Да
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="нет" id="r5" name="r2" />
                                        <label class="form-check-label" for="r5">
                                            Нет
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="btnc">
                        <button type="submit" class="btn btn-outline-primary">Зарегистрировать</button>
                        <button type="reset" class="btn btn-outline-warning">Очистить</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="script.js"></script>
    </body>
</html>
